# MPV-SCREEN-RECORDER

![image](/window.png)

mpv-screen-recorder is a simple Gtk tool, which uses [mpv](https://github.com/mpv-player/mpv) as a desktop recorder.

## INSTALL
    meson build_dir
    ninja -C build_dir install

## DEPENDENCIES

* [mpv-player](https://github.com/mpv-player/mpv)
* python3
* pygobject
