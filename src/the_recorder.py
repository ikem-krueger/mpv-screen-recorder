# =====================================================================
# This file is part of mpv-screen-recorder.
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
import logging
import os
import subprocess
import sys
import time
import socket

from gi.repository import Gdk
from gi.repository import GLib

from .the_grabber import AreaSelector
from mpv_screen_recorder import CHCK_LOG


class ScreenRecorder:
    __ofmt_opt = None
    __vsze_opt = ""
    __fmse_opt = ""
    __shrg_opt = ""
    __rgbd_opt = ""
    __dmse_opt = ""
    __vid_wdth = None
    __vid_heig = None
    __grb_xy = ""
    __target = None

    def __init__(self, ss):

        dspl = Gdk.Display.get_default()
        self.__dspl_nme = dspl.get_name()
        scrn = dspl.get_default_screen()
        snbr = scrn.get_screen_number()
        self.__scrn_nbr = ".%d" % snbr
        self.__scrn_wdth = scrn.get_width()
        self.__scrn_hght = scrn.get_height()

        if os.getenv("XDG_CURRENT_DESKTOP") == "XFCE" or \
                os.getenv("XDG_CURRENT_DESKTOP") == "LXDE":
            self.__scrn_nbr = ""

        self.__flnm_sfx = time.strftime("%F_%T")
        self.__sock_fle = "/var/tmp/.mpv_rec.s"

        self.__settings = ss

    def __do_sett_apply(self):

        fmt_list = [
            "h264",
            "hevc",
            "m4v",
            "matroska",
            "mp4",
            "mpeg2video",
            "ogv",
            "webm",
            "h264"  # set h264 as default if custom format is an empty string
        ]

        mp4_ext = [0, 1, 2, 4]
        mkv_ext = 3
        mpg_ext = 5
        ogv_ext = 6
        web_ext = 7
        ctm_ext = 8

        ss = self.__settings

        self.__fle_fldr = ss.get_string("recordings-folder")
        self.__fle_name = ss.get_string("file-name")
        self.__sec_dlay = ss.get_int("start-delay")
        self.__dr_mouse = ss.get_enum("draw-mouse")
        self.__fl_mouse = ss.get_int("follow-mouse")
        self.__frm_rate = ss.get_double("frame-rate")
        self.__reg_show = ss.get_enum("show-region")
        self.__reg_bord = ss.get_int("region-border")
        self.__vid_size = ss.get_value("video-size")
#        self.__grb_rgon = ss.get_value("grabbing-region")
        otpt_fmt = ss.get_enum("output-format")
        cstm_fmt = ss.get_string("custom-output-format")
        cstm_ext = ss.get_string("custom-extension")

        self.__fle_ext = None
        self.__ofmt_opt = None

        if otpt_fmt in mp4_ext:
            self.__fle_ext = "mp4"
        elif otpt_fmt == mkv_ext:
            self.__fle_ext = "mkv"
        elif otpt_fmt == mpg_ext:
            self.__fle_ext = "mpeg"
        elif otpt_fmt == ogv_ext:
            self.__fle_ext = "ogv"
        elif otpt_fmt == web_ext:
            self.__fle_ext = "webm"
        elif otpt_fmt == ctm_ext:
            self.__ofmt_opt = cstm_fmt
            self.__fle_ext = cstm_ext.strip(".")
            if not self.__ofmt_opt:
                self.__ofmt_opt = fmt_list[otpt_fmt]
            if not self.__fle_ext:
                self.__fle_ext = self.__ofmt_opt

        if not self.__ofmt_opt:
                self.__ofmt_opt = fmt_list[otpt_fmt]

    def __do_cmd_prpr(self):

        self.__fps_opt = "framerate=%.3f" % self.__frm_rate
        if self.__dr_mouse == 0:
            self.__dmse_opt = ",draw_mouse=0"
        if self.__target and self.__reg_show == 1:
            self.__shrg_opt = ",show_region=1"
            if not self.__reg_bord == 3:
                self.__rgbd_opt = ",region_border=%d" % self.__reg_bord

        if self.__vid_wdth > 100 < self.__vid_heig:
            self.__vsze_opt = ",video_size=%dx%d" % (self.__vid_wdth, self.__vid_heig)

        flnm_opt = self.__fle_fldr + "/" + self.__fle_name + "_" + self.__flnm_sfx + "." + self.__fle_ext

        cmd_args = (
            "mpv", "av://x11grab:%s%s%s" % (self.__dspl_nme, self.__scrn_nbr, self.__grb_xy),
            "--demuxer-lavf-o=%s%s%s%s%s%s" % (self.__fps_opt, self.__vsze_opt,
                                               self.__fmse_opt, self.__dmse_opt,
                                               self.__shrg_opt, self.__rgbd_opt),
            "--of=%s" % self.__ofmt_opt,
            "--o=%s" % flnm_opt,
            "--input-ipc-server=%s" % self.__sock_fle
        )

        return cmd_args

    def __do_record(self):

        cmd = self.__do_cmd_prpr()

        if os.path.exists(self.__sock_fle):
            os.unlink(self.__sock_fle)

        subprocess.Popen(cmd)

    def _do_command(self, cmd):

        with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
            try:
                s.connect(self.__sock_fle)
                s.sendall((cmd + "\n").encode())
            except Exception as e:
                logging.basicConfig(filename=CHCK_LOG, format="%(asctime)s\n%(message)s")
                logging.error(e)

        if cmd == "quit":
            os.unlink(self.__sock_fle)

    def _on_target_selected(self, target):
        self.__target = target

        self.__do_sett_apply()

        if target == 2:
            select_area = AreaSelector()
            if not select_area.aborted:
                self.__vid_wdth = select_area.rect.width
                self.__vid_heig = select_area.rect.height
                self.__grb_xy = "+%d,%d" % (select_area.rect.x, select_area.rect.y)
                self.__sec_dlay = 0
            else:
                sys.exit(1)

        elif target == 1:
            posn = "centered"
            if self.__fl_mouse > 0:
                posn = self.__fl_mouse
            self.__fmse_opt = ",follow_mouse=%s" % posn
            self.__vid_wdth = self.__vid_size[0]
            self.__vid_heig = self.__vid_size[1]

        else:
            self.__vid_wdth = self.__scrn_wdth
            self.__vid_heig = self.__scrn_hght

        if self.__sec_dlay > 0:
            GLib.timeout_add_seconds(self.__sec_dlay, self.__do_record)
        else:
            self.__do_record()

    def _do_cmd_check(self):

        self.__do_sett_apply()
        chck_cmd = [
            "mpv", "av://x11grab:%s%s" % (self.__dspl_nme, self.__scrn_nbr),
            "--demuxer-lavf-o=framerate=%.3f" % (self.__frm_rate),
            "--of=%s" % self.__ofmt_opt,
            "--o=/dev/null",
            "--frames=4"
        ]

        try:
            subprocess.check_output(chck_cmd)
        except subprocess.CalledProcessError as e:
            logging.basicConfig(filename=CHCK_LOG, format="%(asctime)s\n%(message)s")
            logging.error(e.output)
            return False

        return True
