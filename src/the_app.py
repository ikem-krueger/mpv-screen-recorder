# =====================================================================
# This file is part of mpv-screen-recorder.
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
from gi.repository import Gtk

from .the_win import AppWindow
from mpv_screen_recorder import APP_ID
from .the_settings import SettingsWindow


class Application(Gtk.Application):

    def __init__(self):

        Gtk.Application.__init__(self, application_id=APP_ID)
        self.__win = None
        self.__minimized = None
        self.__stngsonly = None

    @property
    def stngsonly(self):
        return self.__stngsonly

    @property
    def minimized(self):
        return self.__minimized

    @stngsonly.setter
    def stngsonly(self, arg):
        self.__stngsonly = arg

    @minimized.setter
    def minimized(self, arg):
        self.__minimized = arg

    def do_activate(self):

        if not self.__win:
            self.__win = AppWindow(self)
            self.__win.show()

        if self.__minimized:
            self.__win._on_widgets_toggle()
        else:
            self.__win.present()

        if self.__stngsonly:
            app = SettingsWindow(self.__win)
            self.__win.tray.icon.set_visible(False)
            app.connect("destroy", lambda w: self.__win.close())

        self.__win.connect("delete-event", self.__on_quit_cb)

    def do_startup(self):
        Gtk.Application.do_startup(self)

    def __on_quit_cb(self, win, event):

        if win.record_is_active:
            win._on_widgets_toggle()
            return True

        self.quit()
        return True

# if __name__ == "__main__":
#    app = Application()
#    app.run(sys.argv)
