# =====================================================================
# This file is part of mpv-screen-recorder
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
from gi.repository import Gtk
from mpv_screen_recorder import *
from .the_settings import SettingsWindow


class AppIcon:

    def __init__(self, win):
        self.__icon = Gtk.StatusIcon.new_from_icon_name(REC_ICON)
        self.__icon.set_tooltip_text(_("Record desktop"))
        self.__icon.connect("activate", self.__on_window_show_cb)
        self.__icon.connect("popup-menu", self.__on_right_click_cb)
        self.__win = win

    def __on_right_click_cb(self, icon, button, time):
        menu = Gtk.Menu()
        is_active = self.__win.record_is_active

        itm = Gtk.MenuItem()
        itm.set_label("Settings")
        itm.connect("activate", lambda x: SettingsWindow(self.__win))
        menu.append(itm)

        itm = Gtk.MenuItem()
        ps_btn = self.__win.ps_btn
        itm.set_label(_("Pause"))
        itm.connect("activate", lambda x: ps_btn.set_active(not ps_btn.get_active()))
        menu.append(itm)
        itm.set_sensitive(is_active)

        itm = Gtk.MenuItem()
        itm.set_label(_("About"))
        itm.connect("activate", self.__on_about_cb)
        menu.append(itm)

        itm = Gtk.MenuItem()
        itm.set_label(_("Quit"))
        itm.connect("activate", lambda x: self.__win.app.quit())
        menu.append(itm)
        itm.set_sensitive(not is_active)

        menu.show_all()

        menu.popup(None, None, None, self, button, time)

    @property
    def icon(self):
        return self.__icon

    def __on_window_show_cb(self, icon):
        self.__win.show()
        self.__icon.set_visible(False)

    def __on_about_cb(self, action):
        about_dlg = Gtk.AboutDialog(modal=True, transient_for=self.__win)
        about_dlg.set_program_name(about_dlg.get_program_name())

        about_comment = "%s\n\nGTK+ %d.%d.%d" % \
            (VERSION, Gtk.get_major_version(),
             Gtk.get_minor_version(), Gtk.get_micro_version())

        about_dlg.set_comments(about_comment)
        about_dlg.set_copyright("%s %s." % (CR_YRS, AUTHOR))
        about_dlg.set_logo_icon_name(APP_ICON)
        about_dlg.set_website(REPO)
        about_dlg.set_website_label("GitLab Repository")
        about_dlg.set_license_type(Gtk.License.GPL_3_0)

        about_dlg.connect("response", lambda w, r: about_dlg.destroy())
        about_dlg.show()
