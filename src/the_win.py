# =====================================================================
# This file is part of mpv-screen-recorder
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import Gtk

from enum import IntEnum

from .the_recorder import ScreenRecorder
from .the_icon import AppIcon
from mpv_screen_recorder import *


class TargetFlags(IntEnum):
    TOGGLE_DESKTOP = 0
    TOGGLE_POINTER = 1
    TOGGLE_AREA = 2


class AppWindow(Gtk.ApplicationWindow):
    __dlay_box = None
    __rec_follow_pointer = None
    __rec_selected_area = None
    __record_is_active = False
    __target_data = None

    def __init__(self, app):
        Gtk.ApplicationWindow.__init__(self, application=app,
                                       title=TITLE,
                                       icon_name=REC_ICON)
        self.__tray = AppIcon(self)
        self.__app = app
        self.set_resizable(False)
        self.set_border_width(6)
        self.__settings = Gio.Settings(schema=APP_ID)
        self.__recorder = ScreenRecorder(self.__settings)
        self.connect("show", lambda w: self.__tray.icon.set_visible(False))
        self.connect("key-press-event", self.__on_escape_key_press_cb)
        self.connect("notify::has-toplevel-focus", self.__on_updt_stats)
        rec_posbl = self.__recorder._do_cmd_check()
        self.__cur_fm = self.__settings.get_int("follow-mouse")
        self.__cur_vs = self.__settings.get_value("video-size")
        self.__cur_sd = self.__settings.get_int("start-delay")
#        self.__cur_up = int(self.__cur_vs[1] * 0.4)
        self.__dm_ss = self.__settings.get_enum("draw-mouse") == 1

        if not rec_posbl and not self.__app.stngsonly:
            self.__on_record_impossible()
            self.__app.quit()

        main_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 18)
        main_box.set_border_width(6)
        self.add(main_box)
        main_box.show()

        chld_vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 6)
        main_box.pack_start(chld_vbox, False, False, 0)
        chld_vbox.show()

        img_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        image = Gtk.Image.new_from_icon_name(APP_ICON, Gtk.IconSize.DIALOG)
        img_box.pack_start(image, False, False, 0)
        chld_vbox.pack_start(img_box, False, False, 0)
        image.show()
        img_box.show()

        title = "<b>" + TITLE + "</b>"
        lbl = Gtk.Label.new(title)
        lbl.set_use_markup(True)
        lbl.set_halign(Gtk.Align.CENTER)
        lbl.set_valign(Gtk.Align.CENTER)
        img_box.pack_start(lbl, False, False, 0)
        lbl.show()

        rb_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 6)
        chld_vbox.pack_start(rb_box, False, False, 0)
        rb_box.show()

        rbtn_1 = Gtk.RadioButton.new_with_label_from_widget(None,
                                                            _("Record entire screen"))
        if (self.__rec_follow_pointer or self.__rec_selected_area):
            rbtn_1.set_active(False)

        rbtn_1.connect("toggled", self.__target_toggled_cb, TargetFlags.TOGGLE_DESKTOP)
        rb_box.pack_start(rbtn_1, False, False, 0)
        rbtn_1.show()

        rbtn_2 = Gtk.RadioButton.new_with_label_from_widget(rbtn_1,
                                                            _("Record by following pointer"))
        if self.__rec_follow_pointer:
            rbtn_2.set_active(True)

        rbtn_2.connect("toggled", self.__target_toggled_cb, TargetFlags.TOGGLE_POINTER)
        rb_box.pack_start(rbtn_2, False, False, 0)
        rbtn_2.show()

        rbtn_3 = Gtk.RadioButton.new_with_label_from_widget(rbtn_2,
                                                            _("Record a selected area"))
        if self.__rec_selected_area:
            rbtn_3.set_active(True)

        rbtn_3.connect("toggled", self.__target_toggled_cb, TargetFlags.TOGGLE_AREA)
        rb_box.pack_start(rbtn_3, False, False, 0)
        rbtn_3.show()

        self.__pxls_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 6)
        chld_vbox.pack_start(self.__pxls_box, False, False, 4)
        self.__pxls_box.show()

        if not self.__rec_follow_pointer:
            self.__pxls_box.set_sensitive(False)

        lbl = Gtk.Label.new(_("Number of pixels"))
        lbl.set_tooltip_text(_("When this is specified with 0, the grabbing region \
follows the mouse pointer and keeps the pointer at the center of region, \
otherwise, the region follows only when the mouse pointer reaches within PIXELS \
(greater than zero) to the edge of region."))
        lbl.set_halign(Gtk.Align.START)
        lbl.set_valign(Gtk.Align.CENTER)
        self.__pxls_box.pack_start(lbl, False, False, 0)
        lbl.show()

        cur_up = int(self.__cur_vs[1] * 0.4)
        self.__fm_adj = Gtk.Adjustment.new(self.__cur_fm, 0, cur_up, 1, 10, 0)
        self.__fm_spin = Gtk.SpinButton.new(self.__fm_adj, 1.0, 0)
        self.__fm_spin.connect("value-changed", self._pixels_spin_value_changed_cb)
        self.__pxls_box.pack_end(self.__fm_spin, False, False, 0)
        self.__fm_spin.show()

        self.__dlay_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 6)
        chld_vbox.pack_start(self.__dlay_box, False, False, 4)
        self.__dlay_box.show()

        if self.__rec_selected_area:
            self.__dlay_box.set_sensitive(False)

        lbl = Gtk.Label.new(_("Delay before start"))
        lbl.set_tooltip_text(_("Value in seconds to wait before recording."))
        lbl.set_halign(Gtk.Align.START)
        lbl.set_valign(Gtk.Align.CENTER)
        self.__dlay_box.pack_start(lbl, False, False, 0)
        lbl.show()

        adjust = Gtk.Adjustment.new(self.__cur_sd, 0, 99, 1, 5, 0)
        self.__delay_spin = Gtk.SpinButton.new(adjust, 1.0, 0)
        self.__delay_spin.connect("value-changed", self._delay_spin_value_changed_cb)
        self.__dlay_box.pack_end(self.__delay_spin, False, False, 0)
        self.__delay_spin.show()

        swtch_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.__dm_switch = Gtk.Switch(active=self.__dm_ss)
        self.__dm_switch.connect("notify::active", self._on_incl_pointer_toggled_cb)

        swtch_lbl = Gtk.Label.new(_("Include pointer"))
        swtch_box.pack_start(swtch_lbl, False, False, 0)
        swtch_box.pack_end(self.__dm_switch, False, False, 0)
        chld_vbox.pack_start(swtch_box, False, False, 0)
        swtch_box.show()
        swtch_lbl.show()
        self.__dm_switch.show()

        btn_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 2)
        main_box.add(btn_box)
        btn_box.show()

        size_group = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)

        cncl_btn = Gtk.Button.new_with_label(_("Cancel"))
        cncl_btn.set_tooltip_text(_("Minimize to systray if recording is active,\n otherwise exit."))
        cncl_btn.set_valign(Gtk.Align.CENTER)
        size_group.add_widget(cncl_btn)
        btn_box.pack_start(cncl_btn, False, False, 0)
        cncl_btn.show()
        cncl_btn.connect("clicked", self.__on_cancel_btn_clicked_cb)

        self.__ps_btn = Gtk.ToggleButton.new_with_label(_("Pause"))
        self.__ps_btn.set_tooltip_text(_("Record desktop"))
        self.__ps_btn.set_valign(Gtk.Align.CENTER)
        size_group.add_widget(self.__ps_btn)
        btn_box.pack_start(self.__ps_btn, False, False, 0)
        self.__ps_btn.show()
        self.__ps_btn.connect("toggled", self.__on_pause_btn_toggled_cb)

        rec_btn = Gtk.ToggleButton.new_with_label(_("Record"))
        rec_btn.set_valign(Gtk.Align.CENTER)
        context = rec_btn.get_style_context()
        context.add_class("suggested-action")
        rec_btn.connect("toggled", self.__on_record_btn_toggled_cb)
        size_group.add_widget(rec_btn)
        btn_box.pack_end(rec_btn, False, False, 0)
        rec_btn.show()
        rec_btn.set_can_default(True)
        rec_btn.grab_default()
        self.__ps_btn.set_sensitive(rec_btn.get_active())

    @property
    def ps_btn(self):
        return self.__ps_btn

    @property
    def tray(self):
        return self.__tray

    @property
    def record_is_active(self):
        return self.__record_is_active

    @property
    def app(self):
        return self.__app

    @property
    def fm_adj(self):
        return self.__fm_adj

    def _on_widgets_toggle(self):
        self.__tray.icon.set_visible(True)
        self.hide()

    def _pixels_spin_value_changed_cb(self, button):

        self.__settings.set_int("follow-mouse", button.get_value_as_int())
        if not button == self.__fm_spin:
            self.__fm_spin.set_value(button.get_value_as_int())

    def _delay_spin_value_changed_cb(self, button):

        self.__settings.set_int("start-delay", button.get_value_as_int())
        if not button == self.__delay_spin:
            self.__delay_spin.set_value(button.get_value_as_int())

    def _on_incl_pointer_toggled_cb(self, button, data):

        if button.get_active():
            self.__settings.set_enum("draw-mouse", 1)
        else:
            self.__settings.set_enum("draw-mouse", 0)

        if not button == self.__dm_switch:
            self.__dm_switch.set_active(button.get_active())

    def __on_record_impossible(self):
        dlg = Gtk.MessageDialog(self, 1, Gtk.MessageType.ERROR,
                                Gtk.ButtonsType.CLOSE, _("Record Impossible"))
        msg = "<big>%s</big>" % (_("Recording with these settings is not possible.\n"
                                 "Please change the output format and try again!"))
        dlg.format_secondary_markup(msg)
        dlg.set_transient_for(self)
        dlg.run()
        dlg.destroy()

    def __on_updt_stats(self, w, prp):
        if self.props.has_toplevel_focus:
            self.__cur_fm = self.__settings.get_int("follow-mouse")
            self.__cur_vs = self.__settings.get_value("video-size")
            self.__cur_sd = self.__settings.get_int("start-delay")
            self.__dm_ss = self.__settings.get_enum("draw-mouse") == 1

    def __on_record_btn_toggled_cb(self, button):

        if button.get_active():
            self.__recorder._on_target_selected(self.__target_data)
            button.set_label(_("Stop"))
            self._on_widgets_toggle()
        else:
            self.__recorder._do_command("quit")
            self.__ps_btn.set_active(False)
            self.destroy()

        self.__record_is_active = button.get_active()
        self.__ps_btn.set_sensitive(self.__record_is_active)

    def __target_toggled_cb(self, button, data):

        if button.get_active():
            rec_follow_pointer = (data == TargetFlags.TOGGLE_POINTER)
            rec_selected_area = (data == TargetFlags.TOGGLE_AREA)

            self.__dlay_box.set_sensitive(not rec_selected_area)
            self.__pxls_box.set_sensitive(rec_follow_pointer)

            self.__rec_follow_pointer = rec_follow_pointer
            self.__rec_selected_area = rec_selected_area

            self.__target_data = data

    def __on_pause_btn_toggled_cb(self, button):

        self.__recorder._do_command("cycle pause")

        if button.get_active():
            self._on_widgets_toggle()
            self.__tray.icon.set_from_icon_name(PAUSE_ICON)
            self.__tray.icon.set_tooltip_text("Record is paused")
        else:
            self.__tray.icon.set_from_icon_name(REC_ICON)
            self.__tray.icon.set_tooltip_text("Record desktop")

    def __on_cancel_btn_clicked_cb(self, button):

        if self.__record_is_active:
            self._on_widgets_toggle()
        else:
            self.destroy()

    def __on_escape_key_press_cb(self, widget, event):

        if (event.keyval == Gdk.KEY_Escape):
            widget.destroy()
            return True

        return False
